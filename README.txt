In this file you should include:

Any information you think we should know about your submission
* Is there anything that doesn't work? Why?
* Is there anything that you did that you feel might be unclear? Explain it here.

A description of the creative portion of the assignment
* Describe your feature
* Why did you choose this feature?
* How did you implement it?

1. (10 / 10 Points) User can enter total calorie amount on start up
2. (8 / 8 Points) User can add new food item by name
3. (8 / 8 Points) User can add new food item by calorie
4. (4 / 4 Points) Adding new food items is done in a second activity
5. (5 / 5 Points) Calories remaining is updated with each new food item
6. (5 / 5 Points) Calorie consumed is updated with each new food item
7. (10 / 10 Points) The list of food items displays foods and their respective calories amounts
8. (10 / 10 Points) Color change when calorie count becomes negative
9. (10 / 10 Points) All inputs are filtered and error messages are displayed accordingly
10. (2 / 2 Points) Code is clean and commented
11. (3 / 3 Points) App is visually appealing
12. (15 / 15 Points) Creative portion - design your own feature(s)!

Your app runs perfectly and looks great, but I'm taking off a point because you didn't fill out the readme. If I hadn't talked to you about your app ahead
of time, it would've been difficult for me to find all of the cool stuff you added.

Great job, though!

Total: 89 / 90
