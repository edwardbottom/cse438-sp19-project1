package com.example.calorie_tracker

import android.app.ActionBar
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.text.Editable
import android.text.TextUtils
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ListView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_add_food.*
import kotlinx.android.synthetic.main.total_calories_input.*

class MainActivity : AppCompatActivity() {

    //set global variables
    private var totalCalories : Int = 0
    private var remainingCalories : Int = 0
    private var foodsConsumed = HashMap<String, Int>()
    private var listView: ListView? = null

    //function to display a dialog
    private fun displayDialog(layout: Int) {
        val dialog = Dialog(this)
        dialog.setContentView(layout)

        //select the window to display to dialog in
        val window = dialog.window
        window?.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT)

        //set a click listener for the close button
        dialog.findViewById<Button>(R.id.close).setOnClickListener {
            val value = totalCaloriesInput.text.toString()
            if(value != null){
                remainingCalories = Integer.parseInt(value)
                dialog.dismiss()
            }
        }

        dialog.show()
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //if an intent exists
        if(intent.getStringExtra("foodName") != null){

            //set values from intent
            val submitedName = intent.getStringExtra("foodName")
            val submitedCals = Integer.parseInt(intent.getStringExtra("foodCalories"))

            //set global values from extras in intent
            foodsConsumed = intent.getSerializableExtra("foodsConsumed") as HashMap<String, Int>
            totalCalories = Integer.parseInt(intent.getStringExtra("totalCalories")) + submitedCals
            remainingCalories = Integer.parseInt(intent.getStringExtra("remainingCalories")) - submitedCals

            //display the new data and content
            setContentView(R.layout.activity_main)
            textView2.text = "Calories Remaining: " + remainingCalories.toString()
            textView3.text = "Total Calories: " + totalCalories.toString()

            //store foods in a list of strings
            val keyList = ArrayList(foodsConsumed.keys)
            val valueList = ArrayList(foodsConsumed.values)
            var displayList = arrayOfNulls<String>(keyList.size)
            print(displayList.size.toString() + " is the size of the display list")
            for(i in 0..keyList.size-1){
                displayList[i] = "" + keyList.get(i).toString() + " : " + valueList.get(i).toString() + ""
            }
            displayList.sort()

            //create array adapter to render the page
            val adapter = ArrayAdapter(this,
                R.layout.listview_item, displayList)

            //set array adapter as item list
            val listView:ListView = findViewById(R.id.foodsConsumedList)
            listView.setAdapter(adapter)


            //set text color to red it calories are negative
            if(remainingCalories < 0){
                textView2.setTextColor(Color.RED)
            }

            //set a click listener for the food
            addFoodItem.setOnClickListener(object: View.OnClickListener {
                override fun onClick(view: View){
                    val intent : Intent = Intent(this@MainActivity, AddFood::class.java)
                    intent.putExtra("totalCalories", totalCalories.toString())
                    intent.putExtra("remainingCalories", remainingCalories.toString())
                    intent.putExtra("foodsConsumed", foodsConsumed)
                    startActivity(intent)
                }
            })

            //set up the navigation button for bmi calc
            navBmi.setOnClickListener(object: View.OnClickListener {
                override fun onClick(view: View){
                    val intent : Intent = Intent(this@MainActivity, BmiCalculator::class.java)
                    intent.putExtra("totalCalories", totalCalories.toString())
                    intent.putExtra("remainingCalories", remainingCalories.toString())
                    intent.putExtra("foodsConsumed", foodsConsumed)
                    intent.putExtra("foodName", "")
                    intent.putExtra("foodCalories", "")
                    startActivity(intent)
                }
            })
        }

        //if rendering the total calories input
        else{
            //set the input view
            setContentView(R.layout.total_calories_input)

            //set the submit total listener
            close.setOnClickListener(object: View.OnClickListener {
                override fun onClick(view: View){
                    //alert if value is null
                    if(totalCaloriesInput.text.toString().trim().length == 0){
                        Toast.makeText(this@MainActivity, "Input cannot be blank", Toast.LENGTH_LONG).show()
                    }
                    //else input is valid
                    else{
                        //store input values
                        val totalCal: String = totalCaloriesInput.text.toString()
                        totalCalories = 0
                        remainingCalories = Integer.parseInt(totalCal)

                        //display input values
                        setContentView(R.layout.activity_main)
                        textView2.text = "Calories Remaining: " + remainingCalories.toString()
                        textView3.text = "Total Calories: " + totalCalories.toString()

                        //listener to navigate to add food items
                        addFoodItem.setOnClickListener(object: View.OnClickListener {
                            override fun onClick(view: View){
                                val intent : Intent = Intent(this@MainActivity, AddFood::class.java)
                                intent.putExtra("totalCalories", totalCalories.toString())
                                intent.putExtra("remainingCalories", remainingCalories.toString())
                                intent.putExtra("foodsConsumed", foodsConsumed)
                                startActivity(intent)
                            }
                        })

                        //button to navigate to bmi page
                        navBmi.setOnClickListener(object: View.OnClickListener {
                            override fun onClick(view: View){
                                val intent : Intent = Intent(this@MainActivity, BmiCalculator::class.java)
                                intent.putExtra("totalCalories", totalCalories.toString())
                                intent.putExtra("remainingCalories", remainingCalories.toString())
                                intent.putExtra("foodsConsumed", foodsConsumed)
                                intent.putExtra("foodName", "")
                                intent.putExtra("foodCalories", "")
                                startActivity(intent)
                            }
                        })
                    }
                }
            })

            //set the listener to calculate the total number of reccomended calories
            calculateReccomendedCalories.setOnClickListener(object: View.OnClickListener {
                override fun onClick(view: View){
                    //alert error if input is null
                    if(height.text.toString().trim().length == 0 || weight.text.toString().trim().length ==0){
                        Toast.makeText(this@MainActivity, "Input cannot be blank", Toast.LENGTH_LONG).show()
                    }

                    //else if input is valid
                    else{
                        //get user input
                        val inputHeight: Int = Integer.parseInt(height.text.toString())
                        val inputWeight: Int = Integer.parseInt(weight.text.toString())
                        totalCalories = 0
                        remainingCalories = 67 + 14 * inputWeight + 5 * inputHeight

                        //display input on screen
                        setContentView(R.layout.activity_main)
                        textView2.text = "Calories Remaining: " + remainingCalories.toString()
                        textView3.text = "Total Calories: " + totalCalories.toString()

                        //listener to add food item
                        addFoodItem.setOnClickListener(object: View.OnClickListener {
                            override fun onClick(view: View){
                                val intent : Intent = Intent(this@MainActivity, AddFood::class.java)
                                intent.putExtra("totalCalories", totalCalories.toString())
                                intent.putExtra("remainingCalories", remainingCalories.toString())
                                intent.putExtra("foodsConsumed", foodsConsumed)
                                startActivity(intent)
                            }
                        })

                        //navigation for bmi screen
                        navBmi.setOnClickListener(object: View.OnClickListener {
                            override fun onClick(view: View){
                                val intent : Intent = Intent(this@MainActivity, BmiCalculator::class.java)
                                intent.putExtra("totalCalories", totalCalories.toString())
                                intent.putExtra("remainingCalories", remainingCalories.toString())
                                intent.putExtra("foodsConsumed", foodsConsumed)
                                intent.putExtra("foodName", "")
                                intent.putExtra("foodCalories", "")
                                startActivity(intent)
                            }
                        })
                    }
                }
            })
        }
    }

    override fun onStart() {
        super.onStart()
        Log.d("Android:", "onStart()")
    }







}
