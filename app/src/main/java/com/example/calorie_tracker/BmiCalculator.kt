package com.example.calorie_tracker

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import android.view.View

import kotlinx.android.synthetic.main.activity_bmi_calculator.*
import kotlinx.android.synthetic.main.content_bmi_calculator.*

class BmiCalculator : AppCompatActivity() {

    //global variables
    private var totalCalories : Int = 0
    private var remainingCalories : Int = 0
    private var foodsConsumed = HashMap<String, Int>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //set the content view
        setContentView(R.layout.activity_bmi_calculator)
        setSupportActionBar(toolbar)

        //store intents
        totalCalories = Integer.parseInt(intent.getStringExtra("totalCalories"))
        remainingCalories = Integer.parseInt(intent.getStringExtra("remainingCalories"))
        foodsConsumed = intent.getSerializableExtra("foodsConsumed") as HashMap<String, Int>

        //create listener that displays bmi
        calculateBmi.setOnClickListener(object: View.OnClickListener {
            override fun onClick(view: View) {
                //set height and weight values
                val weight : Double = Integer.parseInt(bmiWeight.text.toString()).toDouble()
                val height : Double = Integer.parseInt(bmiHeight.text.toString()).toDouble()

                //calculate bmi
                val bmi : Double = Math.round(((weight/height/height)*10000.0) * 100.0) / 100.0

                //display bmi with proper formatting
                if(bmi < 18.5){
                    bmiDisplay.text = "Your BMI is: " + bmi.toString() + ". You are underweight"
                    bmiDisplay.setTextColor(Color.RED)
                }
                else if(bmi > 18.5 && bmi < 24.9){
                    bmiDisplay.text = "Your BMI is: " + bmi.toString() + ". You are a normal weight"
                    bmiDisplay.setTextColor(Color.GREEN)
                }
                else{
                    bmiDisplay.text = "Your BMI is: " + bmi.toString() + ". You are overweight"
                    bmiDisplay.setTextColor(Color.RED)
                }


            }
        })

        //return to the home page and transfer data
        leaveBmi.setOnClickListener(object: View.OnClickListener {
            override fun onClick(view: View) {
                val intent : Intent = Intent(this@BmiCalculator, MainActivity::class.java)
                intent.putExtra("foodName", "a")
                intent.putExtra("foodCalories", "0")
                intent.putExtra("totalCalories", totalCalories.toString())
                intent.putExtra("remainingCalories", remainingCalories.toString())
                intent.putExtra("foodsConsumed", foodsConsumed)
                startActivity(intent)
            }
        })
    }

}
