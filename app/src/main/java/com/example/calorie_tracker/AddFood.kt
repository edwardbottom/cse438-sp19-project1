package com.example.calorie_tracker

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import android.util.Log
import android.view.View
import android.widget.Toast

import kotlinx.android.synthetic.main.activity_add_food.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_add_food.*
import kotlinx.android.synthetic.main.total_calories_input.*


class AddFood : AppCompatActivity() {

    //universal variables
    private var totalCalories : Int = 0
    private var remainingCalories : Int = 0
    private var foodsConsumed = HashMap<String, Int>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //set display
        setContentView(R.layout.activity_add_food)
        setSupportActionBar(toolbar)

        //parse input from intents
        totalCalories = Integer.parseInt(intent.getStringExtra("totalCalories"))
        remainingCalories = Integer.parseInt(intent.getStringExtra("remainingCalories"))
        foodsConsumed = intent.getSerializableExtra("foodsConsumed") as HashMap<String, Int>

        //submit food item button listener
        submitFoodItem.setOnClickListener(object: View.OnClickListener {

            override fun onClick(view: View){
                //alert error if user input is null
                if(itemFoodName.text.toString().trim().length == 0 || itemCalories.text.toString().trim().length ==0){
                    Toast.makeText(this@AddFood, "Input cannot be blank", Toast.LENGTH_LONG).show()
                }

                //else the user input is valid
                else{
                    //store vlaues and navigate back to home screen
                    val itemName: String = (foodsConsumed.size + 1).toString() + ". " + itemFoodName.text.toString()
                    val itemCalories: String = itemCalories.text.toString()
                    val itemCaloriesVal = Integer.parseInt(itemCalories)
                    foodsConsumed.put(itemName,itemCaloriesVal)
                    val intent : Intent = Intent(this@AddFood, MainActivity::class.java)
                    intent.putExtra("foodName", itemName)
                    intent.putExtra("foodCalories", itemCalories.toString())
                    intent.putExtra("totalCalories", totalCalories.toString())
                    intent.putExtra("remainingCalories", remainingCalories.toString())
                    intent.putExtra("foodsConsumed", foodsConsumed)
                    startActivity(intent)
                }
            }
        })

        //submit food item button listener
        leaveAddFood.setOnClickListener(object: View.OnClickListener {
            override fun onClick(view: View){
                //store values and navigate back to home screen
                val intent : Intent = Intent(this@AddFood, MainActivity::class.java)
                intent.putExtra("foodName", "name")
                intent.putExtra("foodCalories", "0")
                intent.putExtra("totalCalories", totalCalories.toString())
                intent.putExtra("remainingCalories", remainingCalories.toString())
                intent.putExtra("foodsConsumed", foodsConsumed)
                startActivity(intent)
            }
        })
    }

}

