This application is a basic calorie tracking and health app made in Kotlin with Android Studio. It was designed using
a Pixel emulator and is works best when run using a pixel emulator.

When the app is run, the user is presented with a screen that has two options: the option to enter a total number of
calories to use as the daily number of calories or the option to enter the user's height in cm and weight in kg and then
use their recommended calories for the day.

The recommended calories feature was half of my creative portion. I believe this adds a feature that opens up the user
friendliness of the app and saves newer users from having to look up their recommended amount of calories. This is
valuable if you are like me and probably most other people, and do not know the amount of calories you are recommended
 to eat in a day. Likewise it makes use of data transfer, activities, displays, and forms which were central to this assignment
 and thus justify its addition.

 After choosing to enter either their daily calories manually or calculate their daily calories, the user will be taken to
 the home screen of the app. Once here, the user will see their total calories, their remaining calories, and a list of the
 foods they have consumed. They will also be presented with the option of adding a food item to the list, or of calculating
 their BMI.

 If the user presses the add food button, they will be taken to a screen that will allow them to enter the name of a food
 they eat and the amount of calories in the food. Once they click submit, the food they eat will be appended to the list of
 foods they have eaten and the total calories of the food will be added to the user's total calories consumed and subtracted from
 their remaining calories. If their remaining calories become negative, it will turn red to alert the user that they have exceeded
 their daily reccomended calories.

 Once the user is back on the home screen, they will see the changes they have made reflected in the user interface and will also
 be able to press the calculate BMI button: the other part of my creative portion. Pressing this button will take the user to a
 form where they can input their height in cm and their weight in kg. Note that I have used metrics for this app as I am trying
 to appeal to a much wider audience than just the united states alone. Most of the world is on the metric system, so I have
 decided to target the majority of the world rather than one country. When the user enters their height and weight and presses
 calculate bmi, their bmi will show up at the bottom of the screen in either green text indicating a healthy bmi or red text
 indicating either an over or underweight BMI.

 I believe this feature shows an understanding of how fragments and intents work as well as reapplies existing skills from earlier
 in the assignment such as taking data from forms and changing the color of text. Additionally, if a user downloaded the app with
 the intent of tracking their calories, they most likely want a way to quantify their progress, The addition of a BMI calculator
 allows the user to track their progress and see if they need to gain, loose, or maintain their current weight to maintain a healthy
 lifestyle.

 Once the user has calculated their bmi, they can return to the home screen and continue to track their daily calories or they can
 restart the app and reenter or recalculate their daily calories.